import java.util.*;

public class LongStack {

    private LinkedList<Long> stack;

    public static void main(String[] argum) {
        //interpret(" ");
        //interpret("2 xx");
        //interpret("2 5 - -");
        //interpret("2 3 + 7");
    }

    LongStack() {
        stack = new LinkedList<>();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        LongStack longStack = new LongStack();

        for (Long aLong : this.stack) {
            longStack.push(aLong);
        }
        return longStack;
    }

    public boolean stEmpty() {
        return stack.size() == 0;
    }

    public void push(long a) {
        stack.add(a);
    }

    public long pop() {
        if (stack.size() < 1){
            throw new RuntimeException("Not enough numbers in stack");
        }

        int position = stack.size() - 1;
        long pop = stack.get(position);
        stack.remove(position);
        return pop;
    }

    public void swap(){
        if (this.stack.size() < 2){
            throw new RuntimeException("Not enough elements in stack to SWAP");
        }else{
            long elem1 = this.stack.pop();
            long elem2 = this.stack.pop();
            this.stack.push(elem1);
            this.stack.push(elem2);
        }
    }

    public void rot(){
        if (this.stack.size() < 3){
            throw new RuntimeException("Not enough elements in stack to ROT");
        }else{
            long c = this.stack.pop();
            long b = this.stack.pop();
            long a = this.stack.pop();
            this.stack.push(c);
            this.stack.push(a);
            this.stack.push(b);
        }
    }

    public void op(String s) {
        if (stack.size() < 2){
            throw new RuntimeException("Not enough numbers in stack");
        }

        long arg1 = pop();
        long arg2 = pop();

        if (!(s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/"))){
            throw new RuntimeException("Illegal symbol " + s + " in expression " + arg2 + " " + arg1 + " " + s);
        }

        switch (s) {
            case "+":
                push(arg1 + arg2);
                break;
            case "-":
                push(arg2 - arg1);
                break;
            case "*":
                push(arg1 * arg2);
                break;
            case "/":
                push(arg2 / arg1);
                break;
        }
    }

    public long tos() {
        if (stack.size() < 1){
            throw new RuntimeException("Not enough numbers in stack");
        }

        return stack.get(stack.size() - 1);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LongStack)) return false;
        LongStack longStack = (LongStack) o;
        return Objects.equals(stack, longStack.stack);
    }

    @Override
    public String toString() {
        StringBuilder asString = new StringBuilder();
        for (Long aLong : stack) {
            asString.append(String.valueOf(aLong)).append(" ");
        }

        return asString.toString();
    }

    public static long interpret(String pol) {
        if (pol.trim().isEmpty()) {
            throw new RuntimeException("Empty expression");
        }

        LongStack longStack = new LongStack();
        String[] split = pol.trim().split(" ");

        if (split.length == 1) {
            try {
                return Long.parseLong(split[0]);
            } catch (Exception e) {
                throw new NumberFormatException("Illegal symbol " + split[0] + " in expression " + pol);
            }
        }

        for (String s : split) {
            if (s.equals("SWAP")) {
                if (longStack.stack.size() < 2){
                    throw new RuntimeException("Cannot perform " + s + " in expression " + pol);
                } else {
                    longStack.swap();
                }
            }
            else if (s.equals("ROT")){
                if (longStack.stack.size() < 3){
                    throw new RuntimeException("Cannot perform " + s + " in expression " + pol);
                }
                else {
                    longStack.rot();
                }
            }
            else if (s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/")) {
                if (longStack.stack.size() >= 2) {
                    longStack.op(s);
                } else {
                    throw new RuntimeException("Cannot perform " + s + " in expression " + pol);
                }
            } else if (!(s.isEmpty())) {
                try {
                    longStack.push(Long.parseLong(s.trim()));
                } catch (Exception e) {
                    throw new RuntimeException("Illegal symbol " + s + " in expression " + pol);
                }
            }
        }

        if (longStack.stack.size() == 1) {
            return longStack.pop();
        } else {
            throw new RuntimeException("Too many numbers in expression " + pol);
        }
    }
}

